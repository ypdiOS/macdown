#！ /bin/bash

# app 绝对路径
apppath="/Users/xtkj2018/Desktop/BMYY/BMYY.app"
#依赖库路径
frameworkpath="${apppath}/Contents/Frameworks/"
#Qt插件路径
pluginpath="${apppath}/Contents/PlugIns/"
#开发证书名字
cert="Developer ID Application: Chongqing Ricebug Technology Co., Ltd. (Y3LJXTL24B)"
#安装证书
certInstall="3rd Party Mac Developer Installer: Chongqing Ricebug Technology Co., Ltd. (Y3LJXTL24B)"
#沙盒权限配置文件
entitlementPath="/Users/xtkj2018/Desktop/BMYY/BMYY.entitlements"

#给程序签名
codesign --entitlements ${entitlementPath} -s "${cert}" ${frameworkpath}*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}audio/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}bearer/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}iconengines/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}imageformats/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}mediaservice/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}platforminputcontexts/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}platforms/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}position/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}printsupport/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}sqldrivers/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}styles/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}virtualkeyboard/*
codesign --deep --entitlements ${entitlementPath} --options=runtime -s "${cert}" ${apppath}
