# Apple MacOS 三方应用签名公证流程

从 macOS10.15版本后，从 App Store 之外分发的 App 都需要经过 签名 和 Apple 公证（notarize）,否则会被[“门禁”](https://support.apple.com/zh-cn/HT202491)导致安装包不被信任.

###### 流程
- 开发者证书. 公证证书:`Developer ID Application` 安装证书:`Developer ID Installer` 
- 给App(XXX.app)添加签名. (Ps: app内的所有三方库都需要签名)
- 将签名后的App 打包(zip/pkg/dmg) 上传至Apple审核(正常情况下2分钟左右出结果)
- 审核通过 or 失败

## 资料准备
**MacOS App 安装包** 用于签名的App(XXX.app 常规情况下.app后缀是被隐藏了的).

**开发者证书** 使用`主开发者账号`在开发者中心创建公证证书

![Notarize Screenshot](https://gitlab.com/ypdiOS/macdown/-/raw/master/notarize/res/flow_1.png)

如果安装包是pkg 或者 dmg还需要创建安装证书

![Notarize Screenshot](https://gitlab.com/ypdiOS/macdown/-/raw/master/notarize/res/flow_1_1.png)


## 公证流程

### 配置签名

打开终端执行以下步骤:

· 执行`签名脚步`

```
# app 绝对路径
apppath="/Users/xtkj2018/Desktop/BMYY/BMYY.app"
#依赖库路径
frameworkpath="${apppath}/Contents/Frameworks/"
#Qt插件路径
pluginpath="${apppath}/Contents/PlugIns/"
#开发证书名字
cert="Developer ID Application: XXXXXX., Ltd. (XXXXXXXX)"
#沙盒权限配置文件
entitlementPath="/Users/xtkj2018/Desktop/BMYY/BMYY.entitlements"

#给程序签名
codesign --entitlements ${entitlementPath} -s "${cert}" ${frameworkpath}*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}audio/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}bearer/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}iconengines/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}imageformats/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}mediaservice/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}platforminputcontexts/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}platforms/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}position/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}printsupport/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}sqldrivers/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}styles/*
codesign --entitlements ${entitlementPath} -s "${cert}" ${pluginpath}virtualkeyboard/*
codesign --deep --entitlements ${entitlementPath} --options=runtime -s "${cert}" ${apppath}
```
· 验证APP签名

`spctl  --verbose=4 --assess --type execute XXX.app`

输出结果如下:

```
XXX.app: accepted
source=Unnotarized Developer ID
```

· 查询APP签名状态

`codesign -vv XXX.app `

输出结果如下:

```
XXX.app: valid on disk
XXX.app: satisfies its Designated Requirement
```

· 查询APP签名信息(推荐)

`codesign -dvv XXX.app `


· 打包已签名的App(zip、dmg、pkg均可).这里需要注意的是打包成dmg或者pkg的时候,需要对安装包进行签名
 
	· pkg
	productbuild --component ${apppath} /Applications --sign "${certInstall}" myApplication.pkg
	
	· dmg
	codesign -f -o runtime -s "{certInstall}" -v myApplication.dmg --deep

· 将安装包(zip、dmg、pkg) 上传至Apple公证

```
# 上传公证包
# primary-bundle-id ： Apple开发者账户 团队ID + Bundle ID ，请咨询签名证书创建者
# username : 开发者账号 ，Apple ID，请咨询签名证书创建者
# password : 应用专用密码（登录 appleid.apple.com ，点击 Generate Password，创建一个应用专用密码，这样可以避免暴露真实的密码。），请咨询签名证书创建者
# asc-provider : 证书提供者（ProviderShortname） 一般情况下就是 团队ID，请咨询签名证书创建者
# file : 公证的软件包路径
xcrun altool --notarize-app
             --primary-bundle-id "Y3Lxxxxxx.com.app.xxxx"
             --username "Apple ID"
             --password "专用密钥"
             --asc-provider "xxxxxx"
             --file "./xxxx.dmg"
             
# 查询asc-provider(一般情况下就是 团队ID)
xcrun altool --list-providers -u "Apple ID" -p "专用密钥"
```

· 上传成功后会返回一串字符`RequestUUID `

![Notarize Screenshot](https://gitlab.com/ypdiOS/macdown/-/raw/master/notarize/res/flow_2.png)

	
· 查询公证结果

```
xcrun altool --notarization-info "RequestUUID" --username "Apple ID" --password "专用密钥"
```

![Notarize Screenshot](https://gitlab.com/ypdiOS/macdown/-/raw/master/notarize/res/flow_3.png)

如果公证通过，一会儿就会收到 Apple 发送的邮件.

![Notarize Screenshot](https://gitlab.com/ypdiOS/macdown/-/raw/master/notarize/res/flow_4.png)

这个时候就可以安心使用这个安装包了.

如果未通过，可以通过返回的 LogFileURL 查看错误日志，修复相关问题后重新执行公证流程。

![Notarize Screenshot](https://gitlab.com/ypdiOS/macdown/-/raw/master/notarize/res/flow_5.png)

![Notarize Screenshot](https://gitlab.com/ypdiOS/macdown/-/raw/master/notarize/res/flow_6.png)


## 参考资料
公证失败:

[解决常见的公证问题](https://developer.apple.com/documentation/xcode/notarizing_macos_software_before_distribution/resolving_common_notarization_issues)

[运行时问题解决](https://stackoverflow.com/questions/52911791/hardened-runtime-for-java-and-mojave/55716976#55716976)


公证流程:

[NW.js Mac App 签名公证流程](https://www.cnblogs.com/wx1993/p/13277678.html)

[nwjs mac app签名 - 小咔蹭的个人页面 - OSCHINA](https://my.oschina.net/xiaokaceng/blog/704901)

[mac dmg包签名及公证](https://blog.csdn.net/luoshabugui/article/details/109295413)

[Mac开发-公证流程记录Notarizaiton-附带脚本](https://blog.csdn.net/shengpeng3344/article/details/103369804)
